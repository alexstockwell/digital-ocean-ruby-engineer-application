$ ->
	$document = $(document)
	$window = $(window)

	# Accordions
	$accordions = $('.accordion--link')
	$accordions
		.on 'click', (e) ->
			e.preventDefault()
			$this = $(this)
			$this.next().slideToggle()
		.each (i, el) ->
			$this = $(this)
			# console.log $this.next()
			$this.next().hide()

	# ToC Tabs
	$tabs = $('#toc .tabnav a')
	$tabContent = $('#toc .tabcontent')
	activeClass = "active"
	$tabs.on 'click', (e) ->
		e.preventDefault()
		$this = $(this)
		unless $this.hasClass activeClass
			$tabs.removeClass activeClass
			$this.addClass activeClass
			targetContentID = $this.attr("href")
			console.log targetContentID
			$tabContent.each (i, el) ->
				$el = $(el)
				if $el.is ":visible"
					$el.fadeOut 'fast'
			console.log $(targetContentID)
			$(targetContentID).fadeIn()
	$('#pageorder').hide()

	# Smooth scroll
	$('a[href*=#]:not([href=#]):not(.tabnav__link)').on "click", (e) ->
		if location.pathname.replace(/^\//,'') is this.pathname.replace(/^\//,'') and location.hostname is this.hostname
			targetEl = $(this.hash)
			targetEl = if targetEl.length then targetEl else $('[name=' + this.hash.slice(1) +']')
			if targetEl.length
				e.preventDefault()
				$('html,body').animate({ scrollTop: targetEl.offset().top }, 1000)

	# Charts
	$("#new-tools").highcharts({
		chart: {
			type: "column"
		}
		title: {
			text: "New Apps, Frameworks and Tools used in the Last Year"
		}
		xAxis: {
			categories: [
				"Grunt"
				"Mustache"
				"AngularJS"
				"Guard"
				"Jinja2"
				"imagemagick"
				"Sketch"
				"Paw"
			]
		}
		yAxis: {
			title: {
				text: "Number of Production/OSS Projects that Used Each"
			}
		}
		series: [{
			name: "Projects"
			data: [
				4
				1
				1
				6
				1
				3
				3
				1
			]
		}]
	})

	$("#languages").highcharts({
		chart: {
			type: "column"
		}
		title: {
			text: "Programming Languages Used on Projects in the Last Year"
		}
		xAxis: {
			categories: [
				"Ruby"
				"JS/Coffeescript"
				"PHP/Wordpress"
				"Go"
				"Python"
				"Rails (included in Ruby also)"
				"Middleman (included in Ruby also)"
			]
		}
		yAxis: {
			title: {
				text: "Number of Production/OSS Projects that Used Each"
			}
		}
		series: [{
			name: "Projects"
			data: [
				13
				18
				14
				2
				1
				3
				5
			]
		}]
	})

